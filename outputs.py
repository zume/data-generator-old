import json
import csv

import pandas as pd

def write_to_csv(csv_orders, filename):
    
    '''
    Write orders to csv. Represents "raw customer data" 
    Input: csv_orders -> list of dictionaries 
    '''
    
    # writing to the csv
    myFile = open(filename, 'w')
    with myFile:    
        myFields = ['Location', 'Opened', 'Amount', 'Tax', 'Total']   # columns of the csv
        writer = csv.DictWriter(myFile, fieldnames=myFields)        # write the dict writer 
        writer.writeheader()
        writer.writerows(csv_orders)
    print("Successfully created the file", filename, "with", len(csv_orders), "orders")
    
    # reading the csv just saved into pandas dataframe
    rows_to_show = 10
    print("Showing up to", rows_to_show, "orders")
    df = pd.read_csv(filename)
    
    return df.head(rows_to_show)

def write_to_json(events, filename): 
    '''
    Write data as order events in JSON format. Format that data scientists will use 
    
    Converts a list of dictionaries into json events saved as a text file as filename
    Input: Events is a list of events dictionaries and filename is a string
    Output: Print number of events written and filename 
    '''
    
    n = len(events)
    with open(filename, 'w') as file_json: 
        json.dump(events, file_json)
    
    print("Successfully created the file", filename, "with", n, "events")
    
    with open(filename) as file:
        data = json.load(file)
    
    #print(json.dumps(data, indent=4, sort_keys=False)) # printing a nice format 
    return 


def text_to_dict(parameters):
    """
    Read a text file and convert it into a dictionary of paramaters
    parameters -- .txt file 
    """
    
    d = {}
    with open("class_parameters.txt") as f:
        for line in f:
            line = line.rstrip('\n')
            (key, val) = line.split(": ")
            d[key] = val
        
    # Typecast to appropriate types 
    d["lunch_orders"] = int(d["lunch_orders"])
    d["lunch_mean"] = float(d["lunch_mean"])
    d["lunch_std"] = float(d["lunch_std"])
    d["dinner_orders"] = int(d["dinner_orders"])
    d["dinner_mean"] = float(d["dinner_mean"])
    d["dinner_std"] = float(d["dinner_std"])   
    
    return d 