import matplotlib.pyplot as plt

# Functions used for plotting 

# Goals 
# 1. Show plots of assumptions when data is generated

def plot_orders_over_time(days, num_orders, name):
    '''
    Shows increase in orders over time 
    Input days --> list: days 
          num_orders --> list: number of orders that day 
    '''
    
    plt.plot(days, num_orders)
    plt.title("Number of {} orders over time".format(name))
    plt.ylabel("Number of Orders")
    plt.xlabel("Day")
    plt.show()
    return 

def orders_by_week(weekly_order_samples):
    '''
    Plot the distribution of orders by week 
    '''
    # TO DO 
    return

def orders_by_month(weekly_order_samples):
    '''
    Plot the distribution of orders by month
    '''
    # TO DO 
    return

def order_on_specific_day(single_order_sample, date):
    '''
    Plot the distribution of orders on a specific day 
    '''
    # TO DO 
    return

def all_time_order_times(order_times):
    '''
    Plot the histogram of order times over the entire period 
    '''
    
    plt.plot(days, num_orders)
    plt.title("Number of {} orders over time".format(name))
    plt.ylabel("Number of Orders")
    plt.xlabel("Day")
    plt.show()
    
    return 