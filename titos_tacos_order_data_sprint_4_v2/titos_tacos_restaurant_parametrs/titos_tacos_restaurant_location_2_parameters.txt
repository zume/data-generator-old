name: Tito's Tacos
location: Pioneer Square
lunch_orders: 150
lunch_mean: 11.5
lunch_std: 1.0
dinner_orders: 150
dinner_mean: 18.5
dinner_std: 1.0
weekend_multiplier: 1.2
per_capita_income: 70828
yearly_growth: 0.35 
daily_fluctuation: 0.05 
closed_sat_and_sun: False
competitor_factor: False 
hub_id: 2d085e36-58e2-11ea-b406-acde48001122
date_start: 2019-05-26
date_end: 2020-02-26