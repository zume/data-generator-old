'''
Data Generator
Created: February 14, 2020
Newton Kwan
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import json
from datetime import datetime,timezone
from dateutil import tz
import uuid
import random
import time
import os

def sample_distributions(num_of_orders, mu, sigma):
    '''
    This function takes three parameters, creates a distribution, and samples that
    distribution to create a list of events.

    Ex: [100, 6, 1]
    The function would create a normal distribution with n = 100, mean = 6, and
    standard deviation = 1, sample the distribution 100 times, creating 100 events
    in the format of the data model. It will return a list of dictionaries, where a
    single dictionary is a single event.

    Input: num_of_orders --> integer: number of orders
           mu --> float: mean of the distribution
           sigma --> float: standard deviation of the distribution
    Output: returns a list of dictionaries of length num_of_orders
    '''

    events = []
    samples = []

    # assumptions
    # customer is the same for all events
    customer = "Titos Tacos"

    # the hub is always the same
    hub_id = str(uuid.uuid1())

    # the session is always unique per event

    for i in range(num_of_orders):
        event_dict = {}

        # setting up datetime variables
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()
        utc = datetime.utcnow()
        utc = utc.replace(tzinfo=from_zone)
        local = utc.astimezone(to_zone)

        # part A telemetry event
        correlation_id = str(uuid.uuid1())   # always unique for each event
        customer = "Titos Tacos"             # business customer the row was generated for
        type_of_data = "event"               # default type of data
        version = "1.0.0"                    # default is 1.0.0 until change needed
        source = "telemetry"                 # supports telemetry for now, no enrichment
        UTCtimestamp = str(utc)              # convert utc datetime into a string

        # part B telemetry details
        event_type = "customer_order"        # customer in this sense means the consumer
        customer_id = str(uuid.uuid1())      # ID of a specific customer (meaning consumer)
        session_id = str(uuid.uuid1())       # unique ID of the session, currently always unique
        hub_id = hub_id                      # unique to the restaurant location
        hub_location = None                  # None if it's stationary
        menu_version =  "1.0.0"              # hardcoded to 1.0.0 for now
        experiments = None                   # part of the data model
        localtime = str(local)               # local time as a string
        UTCeventtime = str(utc)              # utc time as a string

        # part C telemetry details
        order_snapshot = []
        hour_sample = round(np.random.normal(mu, sigma, 1)[0], 2)
        number_of_skus = random.randint(1,2) # generate 1 or 2
        if number_of_skus == 1:
            total = 6.0
        else:
            total = 15.50
        for sku_num in range(number_of_skus):
            if sku_num == 0:
                subtotal_sample = 6.0
            else:
                subtotal_sample = 9.50
            d = {"sku": sku_num+1,
                 "amount": 1,
                 "subtotal": subtotal_sample,
                 "total": total,
                 "hour of the day": hour_sample
                 }
            order_snapshot.append(d)

        samples.append(hour_sample)


        # part C telemetry details
        partC_event_details_dict = {"order_snapshot": order_snapshot
                                   }

        # part B telemetry details
        partB_event_details_dict = {"event_type": event_type,           # Every event (part A type) has a event type in part B
                                    "customer_id": customer_id,         # The ID for a specific customer. This is null if a customer is not logged in or we cannot find it
                                    "session_id": session_id,           # An ID that joins a set of events into a specific customer session.
                                    "hub_id": hub_id,                   # The ID for the specific Hub (kiosk, etc.) that is being viewed
                                    "hub_location": hub_location,       # The location of the specific hub. For hub's in which the location does not every change, such as a fixed Kiosk, this field will be null as location can be obtained through a lookup
                                    "menu_version": menu_version,       # You can lookup details about this menu with this id.
                                    "experiments": experiments,         # If a user is in an experiment, it would be represented here in the format experiment:treatment with pipe delimitation.  E.G. if the user was in treatment 2 of a ux experiment titled redbuttons, and treatment 12 of an experiment titled aggressivemarketing, the field would be "redbuttons:2|aggressivemarketing:12
                                    "localtime": localtime,             # The local time this event was generated
                                    "UTCtime": UTCeventtime,            # The utc time this event was generated
                                    "details": partC_event_details_dict
                                   }

        # part A
        event_dict = {"correlation_id": correlation_id,    # The is the id associated with all telemetry events
                      "customer": customer,
                      "type": type_of_data,                # The type of data. Typically "event", which is something generated off of a specific event
                      "version": version,                  # The version of this data
                      "source": source,                    # Where the data was generated. Often "telemetry", but can be "enrichment".
                      "UTCtimestamp": UTCtimestamp,        # A timestamp for when this specific row of data was created
                      "details": partB_event_details_dict  # Details is where part B data and part C data is stored.
                      }

        events.append(event_dict)
    return events, samples


def generate_orders_by_hour(parameters):
    '''
    This function generates events based on distributions parametrized by parameters
    Input: parameters --> list of lists of string parameters in the format [orders, mu, sigma]
    Output: order_events --> list of order_events
            order_samples --> list of lists of order samples
    '''

    order_events = []
    order_samples = []

    for p in parameters:
        num_orders = p[0]
        mu = p[1]
        sigma = p[2]
        events, samples = sample_distributions(num_orders, mu, sigma)
        order_events += events
        order_samples.append(samples)

    return order_events, order_samples

def write_to_file(events, filename):
    '''
    Converts a list of dictionaries into a list of json events, which are
    then saved as a text file named filename
    Input: events --> a list of dictionaries, each of which is an event.
           filename --> string name of the file
    Output: Print confirmation of the number of events written and filename
    '''
    n = len(events)
    with open(filename, 'w') as file_json:
        json.dump(events, file_json)

    print("Successfully created the file", filename, "with", n, "events")
    return


# Generating data
path_to_params = "parameters.txt" # change path
with open(path_to_params) as f:
    raw = f.read().splitlines()[2:]

parameters = []
for p in raw:
    nth_params = list(map(float, p.split(",")))
    nth_params[0] = int(nth_params[0])
    parameters.append(nth_params)

interval = (5, 24) # 6am to 11pm
num_of_bins = 50
dx = (interval[1] - interval[0]) / num_of_bins # interval length / number of bins = bin_width


generate_time_start = time.time()
events, order_samples = generate_orders_by_hour(parameters)
generate_time_taken = time.time() - generate_time_start

print("Time to generate", len(events), "events:", round(generate_time_taken, 3), "seconds")

write_data_start = time.time()
filepath = os.getcwd() + "/"                                   # change this for your own use
filename = "demo_titos_tacos_order_events.txt"                 # change this for your own use
write_to_file(events, filename)
with open(filepath + filename) as file:
    data = json.load(file)

write_data_end = time.time()
write_time_taken = write_data_end - write_data_start
print("Time to write to file:", round(write_time_taken, 3), "seconds")

#print(json.dumps(data, indent=4, sort_keys=False)) # printing a nice format
