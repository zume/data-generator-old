name: Tito's Tacos
location: Columbia Tower
lunch_orders: 210
lunch_mean: 12.5
lunch_std: 1
dinner_orders: 0
dinner_mean: 18.5
dinner_std: 1.2
weekend_multiplier: 1.5 
hub_id: 95b61f50-53aa-11ea-aca1-acde48001122
date_start: 2019-05-26
date_end: 2020-02-26