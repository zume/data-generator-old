name: Tito's Tacos
location: Pioneer Square
lunch_orders: 150
lunch_mean: 12.5
lunch_std: 1
dinner_orders: 180
dinner_mean: 17.5
dinner_std: 1.0
weekend_multiplier: 1.2
hub_id: 2d085e36-58e2-11ea-b406-acde48001122
date_start: 2019-05-26
date_end: 2020-02-26