import pandas as pd

from plotting import plot_orders_over_time
from order_assumptions import sample_time_from_normal
from formatting import single_csv_order, single_customer_order_json_telemetry_event
from outputs import write_to_csv, write_to_json, text_to_dict

 # Create a Restaurant class 

class Restaurant: 
    """
    Create a restaurant object with different parameters. 
    
    This object can generate order data as a csv or event data as JSONs. 
    The purpose is two-fold: 
    1. csv is the "raw data" a 3rd party customer would send us. 
    2. event data as a JSON is to help simulate what a data scientist 
    would receive when building models. 
    
    Each object is one instance of a restaurant. Think: specific Tesla 
    car, not the brand. 
    
    The methods are 
    1) __init__: initializes the parameters of the restaurant 
    2) generate_orders :generates event orders based on distributions 
       parameterized by parameters.    
    """
    
    def __init__(self, parameters):

        '''
        Initialize parameters 
        
        Parameters a dictionary containing key-value pairs for parameters 
        Ex: parameters = {
                "name": "Tito's Tacos", "location": "Columbia Tower", 
                "lunch_orders": 10, "lunch_mean": 12.5, "lunch_std": 1, 
                "dinner_orders": 10, "dinner_mean": 18.5, 
                "dinner_std": 1.2, 
                "hub_id": '95b61f50-53aa-11ea-aca1-acde48001122', 
                "date_start": "2019-07-01", "date_end": "2019-12-31"
            }
        '''
        
        # Initialize both csv and json parameters.  
        self.parameters = parameters
        self.name = parameters["name"]
        self.lunch_orders = parameters["lunch_orders"]
        self.lunch_mean = parameters["lunch_mean"]
        self.lunch_std = parameters["lunch_std"]
        self.dinner_orders = parameters["dinner_orders"]
        self.dinner_mean = parameters["dinner_mean"]
        self.dinner_std = parameters["dinner_std"]
        self.hub_id = parameters['hub_id']
        self.date_start = parameters['date_start']
        self.date_end = parameters['date_end']
        self.date_list = pd.date_range(self.date_start, self.date_end).tolist()
        
        # csv specific 
        # this is a human readable string like 'Columbia Tower' 
        self.location = parameters["location"]  
        
    def generate_orders(self, csv=False, json=True):
        
        """
        Generates restaurant orders orders 
        
        csv -- boolean to return csv orders list of dictionaries 
        (default False)
        json -- boolean to return JSON event list of dictionaries 
        (default True)
               
        """
        
        # Create file with information about assumptions 
        # TO DO 
        
        # Initialize assumptions about meal times 
        # DINNER 
        # Assumption: Dinner orders start at the initial dinner orders 
        dinner_orders = self.dinner_orders 
        # Assumption: Dinner mean is constant everyday 
        dinner_mean = self.dinner_mean 
        # Assumption: Dinner std is constant everyday 
        dinner_std = self.dinner_std 
        
        # LUNCH 
        # Assumption: Lunch orders are constant everyday 
        lunch_orders = self.lunch_orders 
        # Assumption: Lunch mean is constant everyday 
        lunch_mean = self.lunch_mean 
        # Assumption: Lunch std is constant everyday 
        lunch_std = self.lunch_std 
        
        # Initialize data structures for storing events 
        json_events = []     
        csv_orders = []     
        
        # Initialize data structures for plots 
        # only applicable in the jupyter notebooks 
        total_orders = []
        lunch_orders_list = []  
        dinner_orders_list = []  
        
        for order_datetime in self.date_list: 

            # Dinner orders 
            for i in range(dinner_orders):
                # Assumption: Sample order time from a normal distribution 
                hour, minute, second = sample_time_from_normal(dinner_mean, dinner_std)
                order_datetime = order_datetime.replace(hour=hour, minute=minute, second=second)
                # Assumption: Amount of money spent is constant 
                amount = 25.0
                # Assumption: Amount of tax is a constant percentage 
                tax = round(amount * 0.101, 2)
                # Assumption: Total = amount + tax
                total = amount + tax
                # create and insert a single csv order 
                csv_order = single_csv_order(self.location, order_datetime, amount, tax, total)
                csv_orders.insert(0, csv_order) # insert order to front of list for readability
                
                # create and insert a single JSON order
                json_event = single_customer_order_json_telemetry_event(self.name, self.hub_id, 
                                                                        order_datetime, hour, minute, 
                                                                        second, amount, tax, total)
                json_events.insert(0, json_event)
            
            # Lunch orders 
            for i in range(lunch_orders): 
                # Assumption: Sample order time from a normal distribution 
                hour, minute, second = sample_time_from_normal(lunch_mean, lunch_std)
                order_datetime = order_datetime.replace(hour=hour, minute=minute, second=second)
                # Assumption: Amount of money spent is constant 
                amount = 20.0
                # Assumption: Amount of tax is a constant percentage 
                tax = round(amount * 0.101, 2)
                # Assumption: Total = amount + tax
                total = amount + tax
                # create and insert a single csv order 
                csv_order = single_csv_order(self.location, order_datetime, amount, tax, total)
                csv_orders.insert(0, csv_order)
                # create and insert a single JSON order
                json_event = single_customer_order_json_telemetry_event(self.name, self.hub_id, 
                                                                        order_datetime, hour, minute, 
                                                                        second, amount, tax, total)
                json_events.insert(0, json_event)
            
            # Append daily variables for plotting.  
            total_orders.append(dinner_orders + lunch_orders)
            lunch_orders_list.append(lunch_orders)
            dinner_orders_list.append(dinner_orders)
            
            # Update daily variables for next day's plots 
            # Assumption: Dinner orders increase by 1 every day 
            dinner_orders += 1 
            # Assumption: Lunch orders increase by 1 every day 
            lunch_orders += 1 

        # Plotting different aspects of the restaurant 
        # only helpful when using Jupyter notebooks 
        print("Showing metrics about the restaurant")
        plot_orders_over_time(self.date_list, lunch_orders_list, "lunch")   # plots the number of lunch orders overtime 
        plot_orders_over_time(self.date_list, dinner_orders_list, "dinner") # plots the number of dinner orders overtime 
        
        # Return csv orders, json events, or both objects 
        if csv == True and json == False:
            return csv_orders
        elif csv == False and json == True: 
            return json_events 
        elif csv == True and json == True:
            return csv_orders, json_events
        else: 
            return "Error: Raise an exception. csv or json not a boolean."


if __name__ == "__main__":

	# create the instance 
	filename = "class_parameters.txt"
	parameters = text_to_dict(filename)
	titos_tacos_location = Restaurant(parameters)

	# generate orders 
	csv_orders, json_events= titos_tacos_location.generate_orders(csv=True, json=True)

	# write to csv and json 
	write_to_csv(csv_orders, "csv_testing_titos_tacos.csv")
	write_to_json(json_events, "json_testing_titos_tacos.txt")


