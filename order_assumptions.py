import numpy as np 

# Assumptions about the data generated represented as functions 

# Goals 
# 1. Capture the assumptions for each dataset generated
# 2. Reproducible datasets given the same parameters (seeds)
# 3. User specified parameters determine the assumptions

# Order time assumptions 
# ----------------------
def sample_time_from_normal(mean, std):
    """
    Assumption: the order time is distributed normally. 
    
    Given a mean and std, sample a value from a normal distribution 
    and convert that value into hour, day, and second. 
    """

    hour_decimal = round(np.random.normal(mean, std, 1)[0], 2)
    hour_floor = int(np.floor(hour_decimal))
    minute_decimal = (hour_decimal - hour_floor) * 60 
    minute_floor = int(np.floor(minute_decimal))
    second_decimal = (minute_decimal - minute_floor) * 60
    second_floor = int(np.floor(second_decimal))
            
    return hour_floor, minute_floor, second_floor



# Number of order assumptions 
# ---------------------------

# TO DO 
# Holiday event assumptions 
def holiday_dip(orders, date):
    """
    If date is equal to any of these holidays, the adjust orders for that lunch period 
    """

    # Christmas 
    if date = "12/25": 
        adjusted_orders = 0 

    return adjusted_orders
