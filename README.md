# Data Generator

This repository generates synthetic order level data for a restaurant with the goal of eventually generating synthetic data for the entire food supply chain. Currently, it generates orders off of ordering models that use normal distributions with parameters number of events, mean, and standard deviation. Orders are sampled from these distributions and saved to a .csv file following a typical export of data from a Point of Sales (POS) provider like Toast and to a JSON dump following a particular data model for events. Schema documentation is updated regularly and can be found [here](https://zumepizza.atlassian.net/wiki/spaces/TERA/pages/659325236/Examples+of+Customer+Data+throughout+the+Pipeline).

Additionally, there are a series of Jupyter notebooks used for prototyping the generator. The latest version is a more robust experience with useful plots. 

### How do I generate my own data?

* Fork `data_generator`
* Specify parameters in `class_parameters.txt`
* run `python data_generator_v10.py`

### Versions

Detailed version descriptions can be found [here](https://zumepizza.atlassian.net/wiki/spaces/TERA/pages/672038913/Data+Generator)

### Authors

* Newton Kwan (newtonkwan@zume.com)


