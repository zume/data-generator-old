import time
import os
import uuid
from datetime import datetime, timezone, timedelta

from dateutil import tz

# CSV row assumptions
# -------------------
def single_csv_order(location, opened, amount, tax, total):
    ''' 
    Assumption: The order for a restaurant has location, opened, amount, 
    tax, and total 
    Location = human readable string of location like 'Columbia Tower'
    Opened = string of the time an order was begun 
    Amount = amount of money before tax spent on order 
    Tax = amount of money paid in tax 
    Total = amount + tax 
    '''
    order = { 
              "Location": location, 
              "Opened": opened,
              "Amount": amount, 
              "Tax": tax, 
              "Total": total
            }  
    return order

# JSON event assumptions
# ----------------------
def single_customer_order_json_telemetry_event(customer_name, hub_id, 
                                               curr_date, hour, minute, 
                                               second, amount, tax, total):
    '''
    Assumptions
    '''
    event_dict = {} 
            
    # Initializig timezone variables
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()
    utc = datetime.utcnow() 
    utc = utc.replace(tzinfo=from_zone)
    
    # Variables for part A telemetry event
    correlation_id = str(uuid.uuid1())  # creates a unique ID 
    customer = customer_name 
    type_of_data = "event"              
    version = "1.0.0"   # version of the data 
    source = "telemetry"
    UTCtimestamp = str(utc)  # when the event was generated on our end 
    
    # Variables for part B telemetry details
    event_type = "customer_order"
    customer_id = str(uuid.uuid1())  # assumes unique customers
    session_id = str(uuid.uuid1())  # assumes unique sessions 
    hub_id = hub_id  # unique to the restaurant location
    hub_location = None  # None if it's stationary
    menu_version =  "1.0.0"
    experiments = None  # is this an experiment? 
    curr_date = curr_date.replace(hour=hour, minute=minute, 
                                  second=second, 
                                  tzinfo = tz.tzlocal())
    localtime = str(curr_date)  # local time of event on the customer end 
    UTCeventtime = str(curr_date.astimezone(tz.tzutc()))     
    
    # Variables for part C telemetry details 
    order_snapshot = []
    d = {
         "amount": amount,
         "tax": tax,
         "total": total
         }
    order_snapshot.append(d)
            
        
    # part C telemetry details
    partC_event_details_dict = {"order_snapshot": order_snapshot
                                }
            
    # part B telemetry details
    partB_event_details_dict = {"event_type": event_type,           
                                "customer_id": customer_id,         
                                "session_id": session_id,           
                                "hub_id": hub_id,                   
                                "hub_location": hub_location,       
                                "menu_version": menu_version,       
                                "experiments": experiments,         
                                "localtime": localtime,             
                                "UTCtime": UTCeventtime,            
                                "details": partC_event_details_dict
                                }
            
    # part A telemetry event 
    event_dict = {"correlation_id": correlation_id,    
                  "customer": customer,
                  "type": type_of_data,                
                  "version": version,                  
                  "source": source,                    
                  "UTCtimestamp": UTCtimestamp,        
                  "details": partB_event_details_dict  
                  }
    
    return event_dict 